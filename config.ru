$LOAD_PATH << File.join(File.expand_path(File.dirname(__FILE__)), 'lib')
require 'server_app'

use Rack::Deflater
use Rack::CommonLogger

run Sinatra::Application
