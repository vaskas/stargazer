require 'mongo_mapper' 

class Horoscope
  include MongoMapper::Document

  key :day, String
  key :sign, String
  key :language, String
  key :text, String
end