require 'yaml'
require 'fileutils'
require 'time'
require 'singleton'

$LOAD_PATH << File.expand_path(File.dirname(__FILE__))
require 'horoscope'
require 'provider'
require 'providers/astrolis'
require 'providers/oculus'    

class Stargazer
  attr_reader :providers
  
  def self.download_and_save(language, day)
    Provider.class_for_language(language).new.download_and_save(day)            
  end
  
  def initialize
    @providers = %W(ru en).inject({}){ |p, lang| p[lang] = Provider.class_for_language(lang).new; p }
  end
  
  def horoscope(language, day, sign)
    provider = @providers[language]
    raise "Language not implemented" unless provider
    
    horoscope = provider.horoscope(day, sign)
    raise "Horoscope not present for #{day} for #{sign}" unless horoscope
    
    horoscope
  end
  
end                                                                                         

class SingletonStargazer < Stargazer
  include Singleton
end
