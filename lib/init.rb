require 'mongo_mapper'

db_env = {
  'host'     => ENV['MONGO_HOST'],
  'name'     => ENV['MONGO_NAME'],
  'user'     => ENV['MONGO_USER'],
  'password' => ENV['MONGO_PASSWORD'],
  'port'     => ENV['MONGO_PORT']
}
db_config_path = File.join('config','db.yml')
db = File.exists?(db_config_path) ? YAML::load_file(db_config_path)['database'] : db_env

MongoMapper.connection = Mongo::Connection.new(db['host'], db['port'], :pool_size => 5, :timeout => 5)
MongoMapper.database = db['name']
MongoMapper.database.authenticate(db['user'], db['password'])