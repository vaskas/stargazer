require 'init'
require 'sinatra'
require 'stargazer'

def stargazer
  SingletonStargazer.instance
end

get '/:language/:date/:zodiac_sign' do
  begin
    stargazer.horoscope(params[:language], params[:date], params[:zodiac_sign])
  rescue Exception => e
    halt 404, e.message
  end
end

get '/force-download' do
  time = Time.now
  %W(ru en).each do |lang|
    [time, time + 24 * 3600].each do |day|
      Stargazer.download_and_save lang, day.strftime("%Y%m%d")
    end
  end
end