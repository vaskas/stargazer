class Astrolis < Provider
  
  def initialize
    super('en')
  end
  
  def download(day)
    @signs.each do |lat, rus|
      html = `curl -s http://www.astrolis.com/horoscopes/#{lat}`
      html = `curl -s http://www.astrolis.com/horoscopes/#{lat.downcase}`
      raise "HTML not downloaded" if html.empty?
      
      match_data = html.scan(/publishHoroscope\(\'.+\', \'(.+)\', \'(.+)'\)/)
      raise "HTML parsing error" if match_data.nil?
      
      day_string = Time.parse(day).strftime("%A, %B %e").gsub("  "," ")
      horoscope = match_data.select { |m| m[0] == day_string }[0][1].gsub("\\","")
      
      @horoscopes[day]      = {} unless @horoscopes[day]
      @horoscopes[day][lat] = horoscope
    end
  end
end