class Oculus < Provider 
  
  def initialize
    super('ru')
  end
  
  def day_name(day)
    gap = (Time.parse(day) - Time.now) / 3600
    
    return "segodnya" if(gap <= 0 && gap > -24) 
    return "zavtra"   if(gap > 0  && gap <= 24)
    raise  "Requested date not supported by the provider"
  end
  
  def download(day)
    @signs.each do |lat, rus|
      html = `curl -s http://oculus.ru/goroskop_na_#{day_name(day)}/#{rus}.html | iconv -f cp1251 -t utf-8`
      raise "HTML not downloaded" if html.empty?
      
      match_data = html.match(/<div class='oculus-epz-zod'>.+<p>(.+)<\/p>/)
      raise "HTML parsing error" if match_data.nil?
      
      @horoscopes[day]      = {} unless @horoscopes[day]
      @horoscopes[day][lat] = match_data[1]
    end
  end                   
  
end