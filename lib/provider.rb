class Provider
  attr_reader :horoscopes
  
  def self.class_for_language(language)
    case language
      when 'ru'; return Oculus
      when 'en'; return Astrolis
      else raise 'Language not implemented'
    end
  end

  def initialize(language)
    @language   = language
    @config     = YAML::load_file(File.join('config','signs.yml'))
    @signs      = @config['Signs']    
    @horoscopes = {}
  end
  
  def horoscope(day, sign)
    load(day) unless @horoscopes[day]
    @horoscopes[day][sign]
  end
  
  def download_and_save(day)
    download(day)
    save(day)
  end
  
  def save(day)
    @signs.each do |sign, rus|      
      Horoscope.new(:day => day, :sign => sign, :language => @language, :text => @horoscopes[day][sign]).save!
    end
  end
  
  def load(day)
    @horoscopes[day] = {}
      
    @signs.each do |sign, rus|
      record = Horoscope.where(:day => day, :sign => sign, :language => @language).first
      @horoscopes[day][sign] = record ? record.text : nil
    end
  end

end