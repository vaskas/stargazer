$LOAD_PATH << File.expand_path("#{File.dirname(__FILE__)}/..")
require 'lib/stargazer'
require 'lib/provider'
require 'lib/providers/oculus'

RSpec.configure do |config|
  config.color_enabled = true
  config.tty = true
  config.formatter = :documentation
end