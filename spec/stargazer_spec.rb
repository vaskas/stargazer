require 'spec_helper'

describe Stargazer do
  
  describe "start" do
    it "should initialize providers for ru and en" do
      stargazer = Stargazer.new
      stargazer.providers.each do |lang, provider|
        provider.should be_a_kind_of Provider
      end
      
      stargazer.providers.keys.should include('ru', 'en')
    end
  end
  
  describe "get a horoscope" do
    let(:stargazer) { Stargazer.new }
    let(:provider)  { double("provider") }
    before(:each)   { stargazer.providers['ru'] = provider }
    
    it "should return a cached horoscope" do
       provider.stub(:horoscope).and_return("It's a beautiful day today for you.")
       stargazer.horoscope('ru', '20111026', 'leo').should eq "It's a beautiful day today for you."
    end        
    
    it "should raise error if unknown language was requested" do
      expect { stargazer.horoscope('de', '20111026', 'leo') }.to raise_error "Language not implemented"              
    end
    
    it "should raise error if horoscope is not present for that sign or day" do
      provider.stub(:horoscope).and_return(nil)
      expect { stargazer.horoscope('ru', '20150101', 'meerkat') }.to raise_error "Horoscope not present for 20150101 for meerkat"
    end
    
  end
  
  describe "download and save" do
    let(:provider) { double("provider") }
    after(:each)   { Stargazer.download_and_save('ru', '20111021') }
    
    it "should create a provider for the specified language" do
      provider_class = double("provider_class")
      provider_class.stub_chain(:new, :download_and_save)                               
      Provider.should_receive(:class_for_language).and_return(provider_class)            
    end
    
    it "should download and save the horoscopes" do
      Provider.stub(:new) { provider }                                                  
      provider.should_receive(:download_and_save).with('20111021')                       
    end
  end

end