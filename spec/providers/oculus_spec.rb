require 'spec_helper'

describe Oculus do
  let(:oculus) { Oculus.new }
  
  describe "day name" do
    it "should return 'zavtra' for tomorrow's date" do
       tomorrow = Time.now + 24*3600
       oculus.day_name(tomorrow.strftime("%Y%m%d")).should eq "zavtra" 
    end                                                              
    
    it "should return 'segodnya' for today's date" do
      today = Time.now
      oculus.day_name(today.strftime("%Y%m%d")).should eq "segodnya"
    end
    
    it "should raise an error if the day is neither today nor tomorrow" do
      yesterday      = Time.now - 24 * 3600
      two_days_later = Time.now + 55 * 3600
      
      message = "Requested date not supported by the provider"           
      expect { oculus.day_name(yesterday.strftime("%Y%m%d"))     }.to raise_error message
      expect { oculus.day_name(two_days_later.strftime("%Y%m%d"))}.to raise_error message
    end
  end                                                               
end